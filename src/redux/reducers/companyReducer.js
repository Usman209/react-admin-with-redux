import { GET_COMPANIES, NEW_COMPANY, EDIT_COMPANY } from "../constants";

const initialState = [];

const companyReducer = (state = [], action) => {
  switch (action.type) {
    case GET_COMPANIES:
      return {
        ...state,
        ...action.payload,
      };
    case NEW_COMPANY:
    case EDIT_COMPANY:
      return {
        ...action.payload,
      };

    default:
      return state;
  }
};

export default companyReducer;
