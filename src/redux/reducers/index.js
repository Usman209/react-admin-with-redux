import { combineReducers } from "redux";

import authReducer from "./authReducer";
import mainReducer from "./mainReducer";
import companyReducer from "./companyReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  main: mainReducer,
  company: companyReducer,
});

export default rootReducer;
