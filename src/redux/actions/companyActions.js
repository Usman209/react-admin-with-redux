import { NEW_COMPANY, GET_COMPANIES, EDIT_COMPANY } from "../constants";

export const newCompany = (data) => {
  return {
    type: NEW_COMPANY,
    payload: data,
  };
};

export const getCompanies = (current) => {
  return {
    type: GET_COMPANIES,
    payload: current,
  };
};

export const editCompany = (data) => {
  return {
    type: EDIT_COMPANY,
    payload: data,
  };
};
