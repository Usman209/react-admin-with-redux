import Starter from "../views/starter/starter.jsx";
import New from "../views/starter/new.jsx";
import Users from "../views/starter/users.jsx";
import NotFound from "../views/404.jsx";
import Signin from "../views/auth/signin.jsx";
import AddUpdate from "../components/common/AddUpdate.jsx";

var ThemeRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ti-loop",
    component: Starter,
    protected: true,
  },
  {
    path: "/company",
    name: "Company",
    icon: "ti-loop",
    component: New,
    protected: true,
  },
  {
    path: "/add-company",
    name: "Add Company",
    icon: "ti-loop",
    component: AddUpdate,
    protected: true,
  },
  {
    path: "/users",
    name: "Users",
    icon: "ti-loop",
    component: Users,
    protected: true,
  },
  {
    path: "/",
    name: "Dashboard",
    component: Starter,
    hidden: true,
    exact: true,
    protected: true,
  },
  {
    path: "/auth/signin",
    name: "Signin",
    component: Signin,
    hidden: true,
    exact: true,
  },
  {
    path: "/404",
    name: "Not Found",
    icon: "mdi mdi-image-filter-vintage",
    hidden: true,
    public: true,
    component: NotFound,
  },
  {
    pathTo: "/404",
    name: "Not Found",
    redirect: true,
  },
];

export default ThemeRoutes;
