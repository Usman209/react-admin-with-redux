import React from "react";
import { Row, Form, Col, Button } from "react-bootstrap";
import { connect } from "formik";

class AddUpdate extends React.Component {
  state = {};
  constructor(props) {
    super(props);

    this.initialState = {};
    console.log(props);
    const { schema } = this.props;
    Object.keys(schema).forEach((key) => {
      // if (key && !schema[key].exclude)
      this.initialState[key] = schema[key].value;
    });

    this.state = this.initialState;
  }

  handleChange = (event) => {
    const { name, value } = event.target;

    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    // this.setState({ id: this.props.id.value });
    this.props.onFormSubmit(this.state);
    this.setState(this.initialState);
  };

  ListField = () => {
    const formField = [];
    const { schema } = this.props;

    Object.keys(schema).forEach((key) => {
      let el = "";
      let arrEl = [];

      if (schema[key].exclude === undefined) {
        switch (schema[key].type) {
          case "text":
            el = (
              <Form.Group key={key} controlId={key}>
                <Form.Label>{schema[key].label}</Form.Label>
                <Form.Control
                  type={schema[key].type}
                  name={key}
                  value={this.state[key]}
                  onChange={this.handleChange}
                  placeholder={schema[key].label}
                />
              </Form.Group>
            );
            break;
          // case "radio":
          // case "checkbox":
          //   el =
          //   schema[key].label.forEach((e) => {
          //     arrEl.push(
          //         <Form.Label>{schema[key].label}</Form.Label>
          //         <Form.Control
          //           type={schema[key].type}
          //           name={key}
          //           value={this.state[key]}
          //           onChange={this.handleChange}
          //           placeholder={schema[key].label}
          //         />
          //       </Form.Group>
          //     );
          //   });
          //   break;
        }
        formField.push(el);
      }
    });

    return formField;
  };
  render() {
    const { operation, entity, show, onClose } = this.props;

    return (
      <div className={`modal ${show && "is-active"}`}>
        <div className="modal-background"></div>

        <div className="modal-content text-center" style={{ width: "50%" }}>
          <div className="columns is-centered text-center">
            <div className="column text-center">
              <h2>
                {operation} {entity}
              </h2>
              <Row>
                <Col sm={{ span: 6, offset: 3 }}>
                  <Form onSubmit={(e) => this.handleSubmit(e)}>
                    <this.ListField />
                    <Form.Group>
                      <Button variant="success" type="submit">
                        {operation} {entity}
                      </Button>
                    </Form.Group>
                  </Form>
                </Col>
              </Row>
              <button
                className="modal-close is-large"
                onClick={onClose}
                aria-label="close"
              ></button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ companies }) => {
  return {
    companies,
  };
};

export default connect(mapStateToProps, null)(AddUpdate);
