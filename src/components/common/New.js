import React from "react";

const New = (props) =>
  props.show ? (
    <div
      style={{
        position: "absolute",
        top: "0",
        right: "0",
        bottom: "0",
        left: "0",
        backgroundColor: "rgba(0, 0, 0, 0.9)",
      }}
    >
      <button
        className="button is-danger is-outlined"
        onClick={() => props.onClickHandle(false)}
      >
        <span>Delete</span>
        <span class="icon is-small">
          <i class="fas fa-times"></i>
        </span>
      </button>

      {props.children}
    </div>
  ) : null;

export default New;
