import React from "react";
import { Row, Form, Col, Button } from "react-bootstrap";

import img1 from "../../../assets/images/users/1.jpg";
import img2 from "../../../assets/images/users/2.jpg";
import img3 from "../../../assets/images/users/3.jpg";
import img4 from "../../../assets/images/users/4.jpg";

import axios from "axios";

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Input,
  Table,
} from "reactstrap";
import New from "../../common/New";
import AddNew from "../../common/AddUpdate";
import AddUpdate from "../../common/AddUpdate";

const Users = () => {
  const [companies, setCompanies] = React.useState({});
  const [users, setUsers] = React.useState({});
  const [addUpdate, setAddUpdate] = React.useState(false);

  const taskForm = {
    entity: "Company",
    operation: "Add",
    schema: {
      id: {
        value: null,
        exclude: true,
      },
      company_name: {
        label: "Company Name",
        type: "text",
        value: "",
      },
      company_email: {
        label: "Company Email",
        type: "text",
        value: "",
      },
      company_website: {
        label: "Company Website",
        type: "text",
        value: "",
      },
      company_address: {
        label: "Company address",
        type: "text",
        value: "",
      },
      company_phone: {
        label: "Company phone",
        type: "text",
        value: "",
      },
      gender: {
        label: 'Gender',
        // l: ["Male", "Female"],
        type: "radio",
        value: ["male", "female"],
      },
    },
  };

  const initGrid = () => {
    axios
      .get("http://localhost:4000/api/users")
      .then(({ data }) => setCompanies(data));
  };

  React.useEffect(() => {
    initGrid();
  }, []);

  let newTaskForState = {};

  const handleClick = async (task = {}) => {
    setAddUpdate(false);

    if (task) {
      Object.keys(task).forEach((t) => {
        taskForm.schema[t].value = task[t];
      });

      taskForm["operation"] = "Edit";
    }

    newTaskForState = await Object.assign({}, taskForm);
    setUsers(newTaskForState);
    setAddUpdate(true);
  };

  const handleDelete = async (obj) => {
    axios
      .delete(`/company/${obj.id}`)
      .then((response) => {
        initGrid();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const formSubmitHandler = (form) => {
    const req = {
      Add: {
        method: "post",
        url: `/company`,
      },
      Edit: {
        method: "put",
        url: `/company/${form.id}`,
      },
    };

    delete form["id"];

    axios[req[users.operation]["method"]](req[users.operation]["url"], form)
      .then((response) => {
        initGrid();
      })
      .catch((err) => {});
  };

  return (
    /*--------------------------------------------------------------------------------*/
    /* Used In Dashboard-4 [General]                                                  */
    /*--------------------------------------------------------------------------------*/

    <Card>
      <CardBody>
        <div className="d-flex align-items-center">
          <div>
            <CardTitle>Companies of the Month</CardTitle>
            <CardSubtitle>Overview of Latest Month</CardSubtitle>
          </div>
          <div className="ml-auto d-flex no-block align-items-center">
            <div className="ml-auto d-flex">
              <button
                onClick={() => handleClick(null)}
                className="btn btn-primary"
              >
                New
              </button>
              {addUpdate && (
                <AddUpdate
                  {...users}
                  onFormSubmit={(form) => formSubmitHandler(form)}
                />
              )}
            </div>
            <div className="dl">
              <Input type="select" className="custom-select">
                <option value="0">Monthly</option>
                <option value="1">Daily</option>
                <option value="2">Weekly</option>
                <option value="3">Yearly</option>
              </Input>
            </div>
          </div>
        </div>
        <Table className="no-wrap v-middle" responsive>
          <thead>
            <tr className="border-0">
              <th className="border-0">Company Name</th>
              <th className="border-0">Company Email</th>

              <th className="border-0">Company Address</th>

              <th className="border-0">Actions</th>
            </tr>
          </thead>
          <tbody>
            {companies.length ? (
              companies.map((user) => {
                return (
                  <tr key={user.id}>
                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {user.name}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {user.email}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {user.isAdmin}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      {" "}
                      <button onClick={() => handleClick(user)}>Edit</button>
                    </td>
                    <td>
                      <button onClick={() => handleDelete(user)}>Delete</button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td>No Companies Found</td>
              </tr>
            )}
          </tbody>
        </Table>
      </CardBody>
    </Card>
  );
};

export default Users;
