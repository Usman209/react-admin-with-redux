import React from "react";
import { Row, Form, Col, Button, Pagination } from "react-bootstrap";

import img1 from "../../../assets/images/users/1.jpg";
import img2 from "../../../assets/images/users/2.jpg";
import img3 from "../../../assets/images/users/3.jpg";
import img4 from "../../../assets/images/users/4.jpg";

import axios from "axios";

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  Input,
  Table,
} from "reactstrap";
import Pages from "../../common/Pages";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { GET_COMPANIES, NEW_COMPANY } from "../../../redux/constants";

const Tasks = (props) => {
  const [companies, setCompanies] = React.useState({});
  const [tasks, setTasks] = React.useState({});
  const [pages, setPages] = React.useState({});
  const [addNew, setAddNew] = React.useState(false);

  const { getCompanies, newCompany, editCompany } = props;

  // const [recUpdated, setRecordUpdated] = React.useState("plain");

  const taskForm = {
    entity: "Company",
    operation: "Add",
    schema: {
      id: {
        value: null,
        exclude: true,
      },
      company_name: {
        label: "Company Name",
        type: "text",
        value: "",
      },
      company_email: {
        label: "Company Email",
        type: "text",
        value: "",
      },
      company_website: {
        label: "Company Website",
        type: "text",
        value: "",
      },
      company_address: {
        label: "Company address",
        type: "text",
        value: "",
      },
      company_phone: {
        label: "Company phone",
        type: "text",
        value: "",
      },
    },
  };

  const editTask = {
    id: 1,
    company_name: "Company",
    company_email: "mail@email.com",
    company_website: "website",
    company_address: "address",
    company_phone: "12312321",
  };

  const initGrid = () => {
    const current = pages.current || 1;

    axios
      .get(`http://localhost:4000/api/company?page=${current}`)
      .then(({ data: { data } }) => {
        const { count, limit } = data;
        setCompanies(data.rows);
        // getCompanies(data.rows);
        setPages({ count, limit, current });
      });
  };

  React.useEffect(() => {
    initGrid();
  }, [pages.current]);

  let newTaskForState = {};

  // React.useEffect(() => {
  //   // setTasks({ ...newTaskForState });
  // }, [tasks]);

  const handleClick = async (task = {}) => {
    // setAddNew(false);

    if (task) {
      Object.keys(task).forEach((t) => {
        taskForm.schema[t].value = task[t];
      });

      taskForm["operation"] = "Edit";
    }

    newTaskForState = await Object.assign({}, taskForm);

    if (taskForm["operation"] === "Edit") editCompany(newTaskForState);
    else newCompany(newTaskForState);
    // setTasks(newTaskForState);

    // setAddNew(true);
  };

  const handleDelete = async (obj) => {
    axios
      .delete(`/company/${obj.id}`)
      .then((response) => {
        initGrid();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const formSubmitHandler = (form) => {
    const req = {
      Add: {
        method: "post",
        url: `/company`,
      },
      Edit: {
        method: "put",
        url: `/company/${form.id}`,
      },
    };

    delete form["id"];

    axios[req[tasks.operation]["method"]](req[tasks.operation]["url"], form)
      .then((response) => {
        initGrid();
      })
      .catch((err) => {});
  };

  const closeHandler = () => {
    console.log("closed");
    setAddNew(false);
  };

  const onClickHandler = (current) => {
    setPages({ ...pages, current });
    // initGrid();
  };

  return (
    /*--------------------------------------------------------------------------------*/
    /* Used In Dashboard-4 [General]                                                  */
    /*--------------------------------------------------------------------------------*/

    <Card>
      <CardBody>
        <div className="d-flex align-items-center">
          <div>
            <CardTitle>Companies of the Month</CardTitle>
            <CardSubtitle>Overview of Latest Month</CardSubtitle>
          </div>
          <div className="ml-auto d-flex no-block align-items-center">
            <div className="ml-auto d-flex">
              <Link
                to="/add-company"
                className="button is-link"
                // tasks={{ ...tasks }}
              >
                New
              </Link>

              {/* <AddNew
                  {...tasks}
                  onFormSubmit={(form) => formSubmitHandler(form)}
                  show={addNew}
                  onClose={closeHandler}
                /> */}
            </div>
            {/* <div className="dl">
              <Input type="select" className="custom-select">
                <option value="0">Monthly</option>
                <option value="1">Daily</option>
                <option value="2">Weekly</option>
                <option value="3">Yearly</option>
              </Input>
            </div> */}
          </div>
        </div>
        <Table className="no-wrap v-middle" responsive>
          <thead>
            <tr className="border-0">
              <th className="border-0">Company Name</th>
              <th className="border-0">Company Email</th>

              <th className="border-0">Company Address</th>
              <th className="border-0">Company website</th>
              <th className="border-0">Company Phone</th>
              <th className="border-0">Actions</th>
            </tr>
          </thead>
          <tbody>
            {companies.length ? (
              companies.map((company) => {
                return (
                  <tr key={company.id}>
                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_name}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_email}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_address}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_website}
                          </h5>
                        </div>
                      </div>
                    </td>

                    <td>
                      <div className="d-flex no-block align-items-center">
                        <div className="mr-2">
                          <img
                            src={img1}
                            alt="user"
                            className="rounded-circle"
                            width="45"
                          />
                        </div>
                        <div className="">
                          <h5 className="mb-0 font-16 font-medium">
                            {company.company_phone}
                          </h5>
                        </div>
                      </div>
                    </td>
                    <td>
                      <button
                        className="button is-success is-outlined"
                        onClick={() => handleClick(company)}
                      >
                        <span>Edit</span>
                        <span className="icon is-small">
                          <i className="fas fa-edit"></i>
                        </span>
                      </button>
                    </td>
                    <td>
                      <button
                        className="button is-danger is-outlined"
                        onClick={() => handleDelete(company)}
                      >
                        <span>Delete</span>
                        <span className="icon is-small">
                          <i className="fas fa-times"></i>
                        </span>
                      </button>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td colSpan="7">No Companies Found</td>
              </tr>
            )}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan="7">
                <Pagination size={"sm"}>
                  <Pages
                    {...pages}
                    gotoPage={(current) => onClickHandler(current)}
                  />
                </Pagination>
              </td>
            </tr>
          </tfoot>
        </Table>
      </CardBody>
    </Card>
  );
};

const mapStateToProps = ({ companies }) => {
  return {
    companies,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCompanies: () => dispatch({ type: GET_COMPANIES, payload: 1 }),
    newCompany: (taskForm) =>
      dispatch({ type: NEW_COMPANY, payload: taskForm }),
    editCompany: (taskForm) =>
      dispatch({ type: NEW_COMPANY, payload: taskForm }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Tasks);
// export default Tasks;
